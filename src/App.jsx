import React, { useEffect } from "react";
import "./App.css";
import { useRecoilState, useRecoilValue } from "recoil";
import { repos as reposAtom, view as viewAtom } from "./recoil/atoms";
import Menu from "./components/Menu";

function App() {
    const [repos, setRepos] = useRecoilState(reposAtom);
    const view = useRecoilValue(viewAtom); // read only

    useEffect(() => {
        getRepos();
    });

    const getRepos = async () => {
        const url = `https://ghapi.huchen.dev/repositories?since=${view}`;
        const response = await fetch(url);
        const body = await response.json();
        setRepos(
            Object.assign({}, repos, {
                [view]: body,
            })
        );
    };

    return (
        <div className="App">
            <Menu></Menu>
            {repos[view].map((repo) => (
                <div key={repo.url}>
                    <a href={repo.url}>
                        {repo.author} / {repo.name}
                    </a>
                    <div>{repo.description}</div>
                    <div>
                        {repo.stars} stars / {repo.forks} forks
                    </div>
                </div>
            ))}
        </div>
    );
}

export default App;
