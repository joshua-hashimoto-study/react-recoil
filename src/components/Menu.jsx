/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
import { useRecoilState } from "recoil";
import { view as viewAtom } from "../recoil/atoms";

const Menu = () => {
    const viewOptions = ["daily", "weekly", "monthly"];

    const [view, setView] = useRecoilState(viewAtom);

    return (
        <nav className="menu">
            {viewOptions.map((option) => (
                <a
                    href="#"
                    className={`menu-item ${
                        view === option
                    } ? "text-bold" : ""`}
                    onClick={() => setView(option)}
                    key={option}
                >
                    Trending {option}
                </a>
            ))}
        </nav>
    );
};

export default Menu;
